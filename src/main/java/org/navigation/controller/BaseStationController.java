package org.navigation.controller;

import org.navigation.repository.BaseStationRepository;
import org.navigation.response.BaseStationReportResponse;
import org.navigation.service.BaseStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class BaseStationController {

    @Autowired
    BaseStationRepository baseStationService;

    @GetMapping("/{uuid}")
    public BaseStationReportResponse baseStationReport(@PathVariable("uuid") String uuid) {
        var baseStation = baseStationService.findById(uuid);
        if(!baseStation.isPresent()){
            throw new RuntimeException();
        }
        return new BaseStationReportResponse(baseStation.get());
    }
}