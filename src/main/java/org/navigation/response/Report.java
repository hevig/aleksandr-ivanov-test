package org.navigation.response;

import org.navigation.Application;
import org.navigation.model.MobileStationPresenceReport;

import java.math.BigInteger;
import java.time.ZoneId;

public class Report {
    public String mobile_station_id;
    public Float distance;
    public Long timestamp;

    public Report(MobileStationPresenceReport mobileStationPresenceReport) {
        withDistance(mobileStationPresenceReport.calculateDistance())
                .withMobile_station_id(mobileStationPresenceReport.mobileStation.id)
                .withTimestamp(mobileStationPresenceReport.createdAt.atZone(Application.defaultZone).toInstant().toEpochMilli());
    }

    public Report withMobile_station_id(String mobile_station_id) {
        this.mobile_station_id = mobile_station_id;
        return this;
    }

    public Report withDistance(Float distance) {
        this.distance = distance;
        return this;
    }

    public Report withTimestamp(Long timestamp) {
        this.timestamp = timestamp;
        return this;
    }
}
