package org.navigation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.Clock;
import java.time.ZoneId;
import java.time.ZoneOffset;


@SpringBootApplication
public class Application {
    public final static ZoneId defaultZone = ZoneOffset.UTC;
    public static Clock clock = Clock.system(defaultZone);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}