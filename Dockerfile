FROM openjdk:10-jdk

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . /usr/src/app/
RUN cd /usr/src/app

RUN ./gradlew bootJar
RUN cp -a /usr/src/app/build/libs/aleksandr-ivanov-test-1.0-SNAPSHOT.jar /usr

RUN rm -rf /usr/src/app

# ENV Settings
ARG ENV=default
ENV PROFILE $ENV

RUN echo $PROFILE

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar -server -Dspring.profiles.active=$PROFILE /usr/aleksandr-ivanov-test-1.0-SNAPSHOT.jar" ]

EXPOSE 8080