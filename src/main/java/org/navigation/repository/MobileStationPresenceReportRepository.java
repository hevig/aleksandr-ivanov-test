package org.navigation.repository;

import org.navigation.model.MobileStation;
import org.navigation.model.MobileStationPresenceReport;
import org.springframework.data.repository.CrudRepository;


public interface MobileStationPresenceReportRepository extends CrudRepository<MobileStationPresenceReport, String> {
    MobileStationPresenceReport findTop1ByMobileStationIdOrderByCreatedAtDesc(String id);
}
