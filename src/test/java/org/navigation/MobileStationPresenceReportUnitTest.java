package org.navigation;

import org.junit.Test;
import org.navigation.model.BaseStation;
import org.navigation.model.MobileStation;
import org.navigation.model.MobileStationPresenceReport;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MobileStationPresenceReportUnitTest {

    @Test
    public void shouldCalculateDistanceBetweenBaseStationAndMobileStationCorrectly() {
        var mobileStationPresenceReport = new MobileStationPresenceReport()
                .withBaseStation(new BaseStation().withX(4f).withY(5f))
                .withMobileStation(new MobileStation().withLastKnownY(1f).withLastKnownX(1f));

        var distance = mobileStationPresenceReport.calculateDistance();
        assertEquals(5f, distance, 0.001);
    }

}
