package org.navigation.response;

import org.navigation.model.MobileStationPresenceReport;

public class MobileStationLocationResponse {

    public MobileStationLocationResponse(MobileStationPresenceReport mobileStationPresenceReport) {
        mobileId = mobileStationPresenceReport.mobileStation.id;
        x = mobileStationPresenceReport.mobileStationX;
        y = mobileStationPresenceReport.mobileStationY;
    }

    public MobileStationLocationResponse(String mobileStationId) {
        mobileId = mobileStationId;
        error_code = 404;
        error_description = "This mobile station was not registered by any base station";
        return;
    }

    public String mobileId;
    public Float x;
    public Float y;
    public Float error_radius;
    public Integer error_code;
    public String error_description;
}
