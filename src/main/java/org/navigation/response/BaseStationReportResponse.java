package org.navigation.response;

import org.navigation.model.BaseStation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BaseStationReportResponse {

    public BaseStationReportResponse(BaseStation baseStation){
        base_station_id = baseStation.id;
        reports = baseStation.mobileStationPresenceReports.stream()
                .map(Report::new)
                .collect(Collectors.toList());
    }

    public String base_station_id;
    public List<Report> reports;
}
