package org.navigation.service;

import org.navigation.model.BaseStation;
import org.navigation.repository.BaseStationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BaseStationService {

    @Autowired
    BaseStationRepository baseStationRepository;

    public BaseStation save(BaseStation baseStation){
        return baseStationRepository.save(baseStation);
    }

    public List<BaseStation> getAll(){
        return makeCollection(baseStationRepository.findAll());
    }

    public Optional<BaseStation> findById(String id){
        return baseStationRepository.findById(id);
    }

    public static <E> List<E> makeCollection(Iterable<E> iter) {
        var list = new ArrayList<E>();
        for (E item : iter) {
            list.add(item);
        }
        return list;
    }
}
