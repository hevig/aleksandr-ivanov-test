package org.navigation;

import org.junit.Test;
import org.navigation.model.BaseStation;
import org.navigation.model.MobileStation;
import org.navigation.service.BaseStationService;
import org.navigation.service.MobileStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BaseStationControllerIntegrationTest extends IntegrationTestBase {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    BaseStationService baseStationService;

    @Autowired
    MobileStationService mobileStationService;

    @Test
    public void shouldReportMobileStationBecauseItIsInRadiusOfBaseStation() throws Exception {

        var baseStation = new BaseStation().withX(0f).withY(0f).withDetectionRadiusInMeters(100f);
        baseStationService.save(baseStation);
        var mobileStation = new MobileStation().withLastKnownX(1f).withLastKnownY(1f);
        mobileStationService.add(mobileStation);

        this.mockMvc
                .perform(get("/"+baseStation.id))
                .andExpect(status().isOk())
                .andExpect(content().json("{'base_station_id':'"+baseStation.id+"','reports':[{'mobile_station_id':'"+mobileStation.id+"','distance':1.4142135,'timestamp':1459504861000}]}"));
    }

    @Test
    public void shouldNotReportMobileStationBecauseItIsOutOfRadiusOfBaseStation() throws Exception {

        var baseStation = new BaseStation().withX(0f).withY(0f).withDetectionRadiusInMeters(1f);
        baseStationService.save(baseStation);
        var mobileStation = new MobileStation().withLastKnownX(10f).withLastKnownY(10f);
        mobileStationService.add(mobileStation);

        this.mockMvc
                .perform(get("/"+baseStation.id))
                .andExpect(status().isOk())
                .andExpect(content().json("{'base_station_id':'"+baseStation.id+"','reports':[]}"));
    }

    @Test
    public void shouldReport2MobileStationsAndReportBothBecauseBothAreInRadius() throws Exception {

        var baseStation = new BaseStation().withX(0f).withY(0f).withDetectionRadiusInMeters(10f);
        baseStationService.save(baseStation);
        var mobileStation = new MobileStation().withLastKnownX(0f).withLastKnownY(10f);
        mobileStationService.add(mobileStation);
        var mobileStation2 = new MobileStation().withLastKnownX(0f).withLastKnownY(-10f);
        mobileStationService.add(mobileStation2);

        this.mockMvc
                .perform(get("/"+baseStation.id))
                .andExpect(status().isOk())
                .andExpect(content().json("{'base_station_id':'"+baseStation.id+"','reports':[" +
                        "{'mobile_station_id':'"+mobileStation.id+"','distance':10,'timestamp':1459504861000}," +
                        "{'mobile_station_id':'"+mobileStation2.id+"','distance':10,'timestamp':1459504861000}" +
                        "]}"));
    }

    @Test
    public void shouldControlThat2BaseStationCouldReportSameMobileStation() throws Exception {

        var baseStation = new BaseStation().withX(0f).withY(0f).withDetectionRadiusInMeters(10f);
        baseStationService.save(baseStation);

        var baseStation2 = new BaseStation().withX(10f).withY(10f).withDetectionRadiusInMeters(10f);
        baseStationService.save(baseStation2);

        var mobileStation = new MobileStation().withLastKnownX(7f).withLastKnownY(7f);
        mobileStationService.add(mobileStation);

        this.mockMvc
                .perform(get("/"+baseStation.id))
                .andExpect(status().isOk())
                .andExpect(content().json("{'base_station_id':'"+baseStation.id+"','reports':[" +
                        "{'mobile_station_id':'"+mobileStation.id+"','distance':9.899495,'timestamp':1459504861000}" +
                        "]}"));

        this.mockMvc
                .perform(get("/"+baseStation2.id))
                .andExpect(status().isOk())
                .andExpect(content().json("{'base_station_id':'"+baseStation2.id+"','reports':[" +
                        "{'mobile_station_id':'"+mobileStation.id+"','distance':4.2426405,'timestamp':1459504861000}" +
                        "]}"));
    }


}
