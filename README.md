To quickly start project use command `sudo docker-compose up --build`

You could change Dockerfile to play with project, for instance to test with gradle 
you could change line `RUN ./gradlew bootJar` to `RUN ./gradlew test` and then execute
`sudo docker-compose up --build` to play with it.

1. No getters setters used (I do not like them), but I used "with" builder extensively
2. "error_radius": float, - did not understood  why we need that and how could I get it if all 
mobile stations of out the range of base stations should not be registered (I still save them but it is questionable should I?)
3. I mostly rely on integration testing, and use unit testing for testing complex business logic
4. Ton of time gone for initial setup of project (NodeJS is much-much easier to begin with) + 
fixing of bug which caused ci to fall with inconsistent timestamp across different machines 
(Mainly my machine and docker)
5. Note that Gitlab CI pipeline is also working
6. Used approx 7-8 hours. Without 4 item - it took approximately 5-6 hours of pure coding
