package org.navigation;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

import static org.navigation.Application.defaultZone;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Ignore
public class IntegrationTestBase {

    final LocalDateTime REFERENCE_DATE_TIME = LocalDateTime.of(2016, 4, 1, 10, 1, 1, 1);
    final LocalDateTime REFERENCE_DATE_TIME_AFTER_YEARS = LocalDateTime.of(2018, 4, 1, 1, 1, 1, 1);

    @Before
    public void setUp() {
        setFirstMoment();
    }

    void setFirstMoment() {
        Application.clock = Clock.fixed(REFERENCE_DATE_TIME.atZone(defaultZone).toInstant(), defaultZone);
    }

    void setSecondMoment() {
        Application.clock = Clock.fixed(REFERENCE_DATE_TIME_AFTER_YEARS.atZone(defaultZone).toInstant(), defaultZone);
    }

}
