package org.navigation.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class BaseStation {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",strategy = "org.hibernate.id.UUIDGenerator")
    public String id;
    public String name;
    public Float x;
    public Float y;
    public Float detectionRadiusInMeters;

    @OneToMany(mappedBy = "baseStation")
    public List<MobileStationPresenceReport> mobileStationPresenceReports = new ArrayList<>();

    public Boolean canCatchSignalOf(MobileStation mobileStation) {
        var x0 = mobileStation.lastKnownX;
        var y0 = mobileStation.lastKnownY;
        return (x - x0) * (x - x0) + (y - y0) * (y - y0) <= detectionRadiusInMeters * detectionRadiusInMeters;
    }

    public BaseStation withId(String id) {
        this.id = id;
        return this;
    }

    public BaseStation withName(String name) {
        this.name = name;
        return this;
    }

    public BaseStation withX(Float x) {
        this.x = x;
        return this;
    }

    public BaseStation withY(Float y) {
        this.y = y;
        return this;
    }

    public BaseStation withDetectionRadiusInMeters(Float detectionRadiusInMeters) {
        this.detectionRadiusInMeters = detectionRadiusInMeters;
        return this;
    }

}
