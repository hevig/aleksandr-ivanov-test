package org.navigation;

import org.junit.Test;
import org.navigation.model.BaseStation;
import org.navigation.model.MobileStation;
import org.navigation.service.BaseStationService;
import org.navigation.service.MobileStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MobileStationControllerIntegrationTest extends IntegrationTestBase {


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BaseStationService baseStationService;

    @Autowired
    private MobileStationService mobileStationService;

    @Test
    public void shouldReportMobileStationBecauseItIsInRadiusOfBaseStation() throws Exception {
        var baseStation = new BaseStation().withX(0f).withY(0f).withDetectionRadiusInMeters(100f);
        baseStationService.save(baseStation);

        var mobileStation = new MobileStation().withLastKnownX(1f).withLastKnownY(1f);
        mobileStationService.add(mobileStation);

        setSecondMoment();

        mobileStation.withLastKnownX(2f).withLastKnownY(2f);
        mobileStationService.add(mobileStation);

        this.mockMvc
                .perform(get("/location/" + mobileStation.id))
                .andExpect(status().isOk())
                .andExpect(content().json("{'mobileId':'" + mobileStation.id + "','x':2,'y':2}]}"));
    }

    @Test
    public void shouldNotReportMobileStationBecauseItIsOutOfRadiusOfBaseStation() throws Exception {
        var baseStation = new BaseStation().withX(0f).withY(0f).withDetectionRadiusInMeters(1f);
        baseStationService.save(baseStation);

        var mobileStation = new MobileStation().withLastKnownX(3f).withLastKnownY(3f);
        mobileStationService.add(mobileStation);

        setSecondMoment();

        mobileStation.withLastKnownX(10f).withLastKnownY(10f);
        mobileStationService.add(mobileStation);

        this.mockMvc
                .perform(get("/location/" + mobileStation.id))
                .andExpect(status().is4xxClientError())
                .andExpect(content().json("{'mobileId':'" + mobileStation.id + "','x':null,'y':null}]}"));
    }

}
