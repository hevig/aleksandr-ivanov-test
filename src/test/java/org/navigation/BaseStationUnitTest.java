package org.navigation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.navigation.model.BaseStation;
import org.navigation.model.MobileStation;
import org.navigation.service.BaseStationService;
import org.navigation.service.MobileStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BaseStationUnitTest {

    @Test
    public void shouldCheckThatBaseStationShouldCatchMobileStationSignal() {
        var baseStation = new BaseStation().withX(0f).withY(0f).withDetectionRadiusInMeters(10f);
        var mobileStation = new MobileStation().withLastKnownX(0f).withLastKnownY(0f);
        assertTrue(baseStation.canCatchSignalOf(mobileStation));
    }

    @Test
    public void shouldCheckThatBaseStationShouldNotCatchMobileStationSignal() {
        var baseStation = new BaseStation().withX(0f).withY(0f).withDetectionRadiusInMeters(10f);
        var mobileStation = new MobileStation().withLastKnownX(0f).withLastKnownY(11f);
        assertFalse(baseStation.canCatchSignalOf(mobileStation));
    }

    @Test
    public void shouldCheckThatBaseStationCanCatchMobileStationSignalOnNegativeScale() {
        var baseStation = new BaseStation().withX(0f).withY(0f).withDetectionRadiusInMeters(10f);
        var mobileStation = new MobileStation().withLastKnownX(0f).withLastKnownY(-10f);
        assertTrue(baseStation.canCatchSignalOf(mobileStation));
    }

    @Test
    public void shouldCheckThatBaseStationCatchMobileStationSignalOnCornerCase() {
        var baseStation = new BaseStation().withX(0f).withY(0f).withDetectionRadiusInMeters(10f);
        var mobileStation = new MobileStation().withLastKnownX(7.07f).withLastKnownY(7.07f);
        assertTrue(baseStation.canCatchSignalOf(mobileStation));
    }

    @Test
    public void shouldCheckThatBaseStationShouldNotCatchMobileStationSignalIfCornerCaseExceeded() {
        var baseStation = new BaseStation().withX(0f).withY(0f).withDetectionRadiusInMeters(10f);
        var mobileStation = new MobileStation().withLastKnownX(7.08f).withLastKnownY(7.08f);
        assertFalse(baseStation.canCatchSignalOf(mobileStation));
    }
}
