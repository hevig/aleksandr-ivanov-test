package org.navigation.service;

import org.navigation.model.MobileStationPresenceReport;
import org.navigation.repository.MobileStationPresenceReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MobileStationPresenceReportService {

    @Autowired
    MobileStationPresenceReportRepository mobileStationPresenceReportRepository;

    public MobileStationPresenceReport getLastMobileStationReportBy(String mobileStationId){
        return mobileStationPresenceReportRepository.findTop1ByMobileStationIdOrderByCreatedAtDesc(mobileStationId);
    }

}
