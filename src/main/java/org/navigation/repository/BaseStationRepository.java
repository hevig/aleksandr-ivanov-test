package org.navigation.repository;

import org.navigation.model.BaseStation;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


public interface BaseStationRepository extends CrudRepository<BaseStation, String> {
}
