package org.navigation.repository;

import org.navigation.model.BaseStation;
import org.navigation.model.MobileStation;
import org.springframework.data.repository.CrudRepository;


public interface MobileStationRepository extends CrudRepository<MobileStation, String> {
}
