package org.navigation.controller;

import org.navigation.model.MobileStationPresenceReport;
import org.navigation.response.MobileStationLocationResponse;
import org.navigation.service.MobileStationPresenceReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MobileStationController {

    @Autowired
    MobileStationPresenceReportService mobileStationPresenceReportService;

    @GetMapping("/location/{uuid}")
    public Object location(@PathVariable("uuid") String mobileStationId) {
        var mobileStationPresenceReport =
                mobileStationPresenceReportService.getLastMobileStationReportBy(mobileStationId);
        if (mobileStationPresenceReport == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MobileStationLocationResponse(mobileStationId));
        }
        return new MobileStationLocationResponse(mobileStationPresenceReport);
    }
}