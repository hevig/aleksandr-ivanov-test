package org.navigation.service;

import org.navigation.model.MobileStation;
import org.navigation.model.MobileStationPresenceReport;
import org.navigation.repository.MobileStationPresenceReportRepository;
import org.navigation.repository.MobileStationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MobileStationService {

    @Autowired
    BaseStationService baseStationService;

    @Autowired
    MobileStationRepository mobileStationRepository;

    @Autowired
    MobileStationPresenceReportRepository mobileStationPresenceReportRepository;

    public void add(MobileStation mobileStation){
        mobileStationRepository.save(mobileStation);

        var baseStationsWhichCanCatchSignalOfMobileStation = baseStationService.getAll()
                .stream()
                .filter(baseStation -> baseStation.canCatchSignalOf(mobileStation));

        baseStationsWhichCanCatchSignalOfMobileStation
                .map(baseStation -> new MobileStationPresenceReport()
                        .withMobileStation(mobileStation).withBaseStation(baseStation))
                .forEach(mobileStationPresenceReport -> mobileStationPresenceReportRepository.save(mobileStationPresenceReport));
    }

}
