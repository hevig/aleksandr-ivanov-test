package org.navigation.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class MobileStation {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",strategy = "org.hibernate.id.UUIDGenerator")
    public String id;
    public Float lastKnownX;
    public Float lastKnownY;

    @OneToMany(mappedBy = "mobileStation")
    public List<MobileStationPresenceReport> mobileStationPresenceReports = new ArrayList<>();

    public MobileStation withId(String id) {
        this.id = id;
        return this;
    }

    public MobileStation withLastKnownX(Float lastKnownX) {
        this.lastKnownX = lastKnownX;
        return this;
    }

    public MobileStation withLastKnownY(Float lastKnownY) {
        this.lastKnownY = lastKnownY;
        return this;
    }
}
