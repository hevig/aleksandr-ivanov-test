package org.navigation.model;

import org.hibernate.annotations.GenericGenerator;
import org.navigation.Application;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class MobileStationPresenceReport {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    public String id;
    public LocalDateTime createdAt = LocalDateTime.now(Application.clock);
    public Float mobileStationX;
    public Float mobileStationY;

    @ManyToOne
    public MobileStation mobileStation;

    @ManyToOne
    public BaseStation baseStation;

    public Float calculateDistance() {
        var x = baseStation.x;
        var y = baseStation.y;
        var x1 = mobileStationX;
        var y1 = mobileStationY;
        return (float) Math.sqrt(Math.pow(x - x1, 2) + Math.pow(y - y1, 2));
    }

    public MobileStationPresenceReport withId(String id) {
        this.id = id;
        return this;
    }

    public MobileStationPresenceReport withMobileStation(MobileStation mobileStation) {
        this.mobileStation = mobileStation;
        this.mobileStationX = mobileStation.lastKnownX;
        this.mobileStationY = mobileStation.lastKnownY;
        return this;
    }

    public MobileStationPresenceReport withBaseStation(BaseStation baseStation) {
        this.baseStation = baseStation;
        return this;
    }
}
